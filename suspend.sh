#!/bin/bash
#Show Dialog
kdialog --title "Suspend" --warningcontinuecancel "Enter Gx:G1(Suspend/Sleep) state?"
#Debug
#export EXIT_DIALOG=$?
if [ "$?" -eq "0" ]; then
	#Debug
	#export SUSPEND_CHAIN="yes"
	
	#Check if OBS is running
	if pgrep -x "obs" > /dev/null 
	then
		#Debug
		#export OBS_RUNNING=$(pgrep -x "obs")
		
		#If it is then kill it.
		kill -s HUP $(pgrep -x "obs")
	fi
	#Suspend/Enter Gx:G1 state
	kshutdown -S
fi
#Used for debug
#kdialog --msgbox "Dialog exit code:$EXIT_DIALOG \nEnter Suspend chain:$SUSPEND_CHAIN\nOBS Running:$OBS_RUNNING"
