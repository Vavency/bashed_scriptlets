 
#!/bin/bash

search__dir_maxdepth='5'
list=($(find -maxdepth "$search__dir_maxdepth"))


# IFS='\n' read -r -a array <<< "$list"

for element in "${list[@]}"
do
	if [[ -f "$element" ]]; then
		processed_cat=$(cat "$element" | grep "$1")
		if [ -z "$processed_cat" ]; then
			donothing=""
		else
			echo  "$element"
			echo "$processed_cat"
		fi
	fi
done
