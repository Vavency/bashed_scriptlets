#!/bin/sh

export A_CARD="alsa_output.pci-0000_00_14.2.analog-stereo"

pactl load-module module-null-sink sink_name=Game_Sink
pactl load-module module-null-sink sink_name=Discord_Sink
pacmd update-sink-proplist Game_Sink device.description="Game_Sink_D"
pacmd update-sink-proplist Discord_Sink device.description="Discord_Sink_D"

pactl load-module module-loopback source=Game_Sink.monitor sink=$A_CARD
pactl load-module module-loopback source=Discord_Sink.monitor sink=$A_CARD
