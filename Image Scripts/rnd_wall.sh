#!/bin/bash

# PREAMBLE:
# This script requires:
# ImageMagick 
# feh


#idk why loops only function outside of functions
for var in "$@"; do
	shargs=$shargs" "$var
done

for var in "$@"; do
	if [[ $var == *"--aspect-ratio="* ]]; then
		aspect_ratio=$(echo $var | tr "=" "\n" | tail -1)
	fi
done

for var in "$@"; do
	if [[ $var == *"--workdir="* ]]; then
		workdir=$(echo $var | tr "=" "\n" | tail -1)
		if [ ! -d "$workdir" ]; then
			echo "Not a folder, get some --help. Exiting."
			exit 1
		fi
		#echo "$workdir"
	fi
done

for var in "$@"; do
	if [[ $var == *"--screensize="* ]]; then
		screensize=$(echo $var | tr "=" "\n" | tail -1)
		#echo "$screensize"
		if [ -z "$screensize" ]; then
			echo "No screen size found. Get some --help. Exiting."
			exit 1
		fi
	fi
done

#setup functions
function parse_shargs {
	if [[ "$shargs" == *"--help"* ]]; then
		echo "'$(echo $0 | tr "/" "\n" | tail -1)' is a script for setting random wallpapers"
		echo ""
		echo "	--recursive	 	Deep lookup in nester directories"
		echo "	--aspect-ratio		Images with equal or greater aspect ratios will be fitted and cropped"
		echo "	--confirm_selection	Asks if you want to set or leave the current image."
		echo "				NOTE: Don't use without terminal access."
		echo "	--leave-temp-img	Leaves temporary image in '"$temp_file_dir"'"
		echo "	--workdir		Point to where to look in"
		echo "				Usage: '--workdir=\"path/to/directory\"' or '--workdir=\"/path/to/directory\"'"
		echo "				Example: --workdir=\"$(pwd)\""
		echo "				Don't forget quotes."
		echo "	--screensize		Overrides automated screen size with a custom one. Useful for multi-monitor setups."
		echo "				Usage: '--screensize=\"WIDTHxHEIGHT\"'"
		echo "				Example: --screensize=\"2560x1440\""
		echo "				Don't forget quotes."
		exit 0
	fi
	
	if [[ "$shargs" == *"--force-aspect-ratio"* ]]; then
		force_aspect_ratio="1"
	fi
	
 	if [[ "$shargs" == *"--debug-output"* ]]; then
		debug_testecho="1"
 	fi
 	
 	if [[ "$shargs" == *"--recursive"* ]]; then
		lookin_subdirectories="1"
	fi
	
	if [[ "$shargs" == *"--confirm_selection"* ]]; then
		confirm_selection=="1"
	else
		rnd_wall_dont_setroot_window="1"
	fi
	
	if [[ "$shargs" == *"--leave-temp-img"* ]]; then
		img_leavetmp="1"
	fi
}

function find_file {
	#Finding random file. (find in workdir of type file | pipe | sort in a random order | pipe | take last line)
	case $lookin_subdirectories in
		"1" )
			img_path=$(find -L "$workdir" -type f \( ! -iname "*~" \) | sort -R | tail -1) ;;
		* )
			img_path="$workdir""/"$(ls "$workdir" -Lq1p | grep -v / | sort -R | tail -1) ;;
	esac
	
	if [ -z "$img_path" ]; then
		echo "Directory is empty... Maybe point to (or run this script in) a directory with image files?"
		exit 1
	fi
}

function get_dom_colour {
	#Getting dominant colour
	img_color=$(convert "$img_path"'[8196@]' +dither -colors 1 -define histogram:unique-colors=true -format "%c" histogram:info:| tr " " "\n" | grep "#")
	
	case $? in
		"0" )
		;;
		* )
		echo ""
		echo "Is this even a image file..."
		echo "Offending file: ""$img_path"
		
		if [ ! -z $debug_testecho ]; then
		    echo "Tried to execute: " "convert \"$img_path\"'[8196@]' +dither -colors 1 -define histogram:unique-colors=true -format \"%c\" histogram:info:| tr \" \" \"\n\" | grep \"#\""
		fi
		
		debug_output
		
		exit 1 ;;
	esac
}

function get_img_resratio {
	#Finding image ratio.
	img_ratio=$(identify -format "%[fx:abs((w/h))]" "$img_path")
}

function get_screenres {
	if [ -z "$screensize" ]; then
		#Getting screen size NOTE: could not work in wayland and does not work with multi-monitor setups.
		#Use '--screensize' to change it to the correct size.
		screensize=$(xdpyinfo | awk '/dimensions/{print $2}')
	fi
}

function setup_defaults {
	if [ -z "$workdir" ]; then
		workdir=$(pwd)
	fi

	if [ -z "$temp_file_dir" ]; then
		#Where I leave final image
		temp_file_dir="/var/tmp/rnd_wall"
	fi
	
	if [ -z $aspect_ratio ]; then
		aspect_ratio="1.2"
	fi

	if [ ! -d "$temp_file_dir" ]; then
		mkdir -p $temp_file_dir
	fi

	#Setting processing and final(temp) file paths
	if [ -z "$img_leavetmp" ]; then
		img_tmp_file=$temp_file_dir"/img_tmp.png"
	else	
		img_tmp_file=$temp_file_dir"/img.png"
	fi
	img_process=$temp_file_dir"/img_process.png"
}

function img_prep {
	#Image processing.
	if (( $(echo "$img_ratio >= $aspect_ratio" | bc -l ) ));then
		convert "$img_path"'['$screensize'^]' -gravity center -extent $screensize -background $img_color -mosaic +matte $img_tmp_file
	else
		#For some bizzare reason I have to write to a file
		convert "$img_path"'[8192@]' -background $img_color -mosaic +matte $img_process
		convert $img_process -blur 10x10 -resize $screensize^ -gravity center -extent $screensize \
		"$img_path" -resize $screensize -gravity center -geometry +0+0 -composite $img_tmp_file
		rm $img_process
	fi
}

function img_set_wallpaper {
	#echo "Setting root window."
	while [ ! -z $confirm_selection ]; do
		read -p "$img_path: (P)review, (C)onfirm, (A)bandon:" -n1 read_input
		echo ""
		case $read_input in
			"a" )
				confirm_selection=""
				rnd_wall_dont_setroot_window="";;
			"p" )
				feh $img_tmp_file;;
			"c" )
				confirm_selection=""
				rnd_wall_dont_setroot_window="1";;
		esac
	done
	if [ ! -z $rnd_wall_dont_setroot_window ]; then
		feh --bg-fill $img_tmp_file
	fi
}

function debug_output {
	#DEBUG
	if [ ! -z $debug_testecho ]; then
		echo "lookin_subdirectories:	$lookin_subdirectories"
		echo "confirm_selection:	$confirm_selection"
		echo "workdir:		$workdir"
		echo ""
		echo "shargs:			$shargs"
		echo ""
		echo "img_path:		$img_path"
		echo "img_color:		$img_color"
		echo "img_ratio:		$img_ratio"
		echo "img_tmp_file:		$img_tmp_file"
		echo ""
		echo "force_aspect_ratio:	$force_aspect_ratio"
		echo "aspect_ratio:		$aspect_ratio"
		echo "screensize:		$screensize "
		echo "pwd:			$(pwd)"
	fi
}

function cleanup {
	#Clean up.
	if ! (( $img_leavetmp )); then
		rm $img_tmp_file
	fi

	workdir=""
	aspect_ratio=""
	img_path=""
	img_tmp_file=""
	img_color=""
	img_ratio=""
	iswide=""
	screensize=""
}

parse_shargs
setup_defaults
find_file
get_dom_colour
get_img_resratio
get_screenres
img_prep
img_set_wallpaper

debug_output

cleanup
