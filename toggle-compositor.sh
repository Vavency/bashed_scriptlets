#!/bin/bash
pid=$(pidof compton)
if [ $pid ]; then
	echo "Disabling compositor"
	killall compton
else

	GLSL_RUN_TEMP="/dev/shm/fake-transparency-fshader-win.run.glsl"
	OPACITY_VALUE=0.3
	FILTER_BIAS=0.3
	COLOUR_FILTER_VALUE='0.0, 0.0, 0.0'


	echo "Enabling compositor"
	#$HOME/gitdown/compton_EvilPuding/compton --frame-opacity 0.999
	#$HOME/gitdown/compton_EvilPuding_OG/compton --backend glx --glx-use-copysubbuffermesa --glx-fshader-win "$(cat $HOME/.compton_shaders/fake-transparency-fshader-win.glsl)" &
	cp $HOME/.compton_shaders/per/fake-transparency-fshader-win.fab.glsl $GLSL_RUN_TEMP
	sed -i -e "s/opacity_placeholder/$OPACITY_VALUE/g" $GLSL_RUN_TEMP
	sed -i -e "s/colour_filter_placeholder/$COLOUR_FILTER_VALUE/g" $GLSL_RUN_TEMP
	sed -i -e "s/filter_bias_placeholder/$FILTER_BIAS/g" $GLSL_RUN_TEMP
	$HOME/gitdown/compton_EvilPuding_OG/compton -c --detect-rounded-corners --no-dock-shadow --force-win-blend --shadow-red 1.0 --shadow-green 0.5686274509803922 --shadow-blue 0.7803921568627451 -r 10 -o 2.0 --backend glx --glx-fshader-win "$(cat $GLSL_RUN_TEMP)" &
	#cat $GLSL_RUN_TEMP
	
	sleep 1.0
	
	rm $GLSL_RUN_TEMP
fi
